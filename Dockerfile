FROM node:16.15

WORKDIR /app

COPY package*.json ./

#test 
RUN node -v
RUN npm --version
# RUN npm install -g npm@8.8.0
#install
RUN npm install

#deploy
COPY . .

# COPY /app/dist ./dist

CMD ["npm", "run", "start"]