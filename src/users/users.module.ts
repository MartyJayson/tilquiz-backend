import { forwardRef, Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { AuthModule } from 'src/auth/auth.module';
import { Lesson } from 'src/lessons/lessons.model';
import { LessonsModule } from 'src/lessons/lessons.module';
import { Role } from 'src/roles/roles.model';
import { RolesModule } from 'src/roles/roles.module';
import { UserRoles } from 'src/roles/user-roles.model';
import { UsersController } from './users.controller';
import { User } from './users.model';
import { UsersService } from './users.service';

@Module({
  controllers: [UsersController],
  providers: [UsersService],
  imports: [
    SequelizeModule.forFeature([User, Role, UserRoles, Lesson]),
    RolesModule,
    LessonsModule,
    forwardRef(() => AuthModule)  
  ],
  exports: [
    UsersService,
  ]
})
export class UsersModule {}
