import { IsNumber, IsString, Length } from "class-validator";

export class addRoleDto{
    @IsString({message: 'Должно быть строкой'})
    @Length(4, 100, {message: 'Не меньше 4 и не больше 100'})
    readonly value: string;
    @IsNumber({}, {message: 'Должно быть числом'})
    readonly userId: number;
}