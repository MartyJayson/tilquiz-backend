import { IsString, Length } from "class-validator";

export class StringDto{
    @IsString({message: 'Должно быть строкой'})
    readonly email:string;
}