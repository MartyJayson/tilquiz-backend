import { ApiProperty } from "@nestjs/swagger";
import { BelongsToMany, Column, DataType, Model, Table } from "sequelize-typescript";
import { Lesson } from "src/lessons/lessons.model";
import { UserLessons } from "src/lessons/user-lessons.model";
import { Role } from "src/roles/roles.model";
import { UserRoles } from "src/roles/user-roles.model";

interface UserCreationAttr{
    email: string,
    password: string
}


@Table({tableName:'users'})
export class User extends Model<User, UserCreationAttr>{
    @ApiProperty({example:'1', description:'Уникальный идентификатор'})
    @Column({type: DataType.INTEGER, unique: true, autoIncrement: true, primaryKey: true})
    id: number;
    
    @ApiProperty({example:'example@mail.com', description:'Почтовый ящик'})
    @Column({type: DataType.STRING, unique: true, allowNull:false})
    email:string;

    @ApiProperty({example:'Джон', description:'Имя'})
    @Column({type: DataType.STRING, allowNull:false})
    name:string;

    @ApiProperty({example:'Джонсон', description:'Фамилия'})
    @Column({type: DataType.STRING, allowNull:false})
    surname:string;

    @ApiProperty({example:'123456', description:'Пароль'})
    @Column({type: DataType.STRING, allowNull:false})
    password:string;
    
    @ApiProperty({example:'true', description:'Забанен или нет'})
    @Column({type: DataType.BOOLEAN, defaultValue: false})
    banned:boolean;
    
    @ApiProperty({example:'Нарушение правила №1', description:'Причина блокировки'})
    @Column({type: DataType.STRING, allowNull:true})
    banReason:string;

    @ApiProperty({example:'Токен', description:'Временный токен для подтверждения'})
    @Column({type: DataType.STRING, allowNull:true})
    token:string;

    @BelongsToMany(() => Role, ()=>UserRoles)
    roles:Role[];

    @BelongsToMany(() => Lesson, ()=>UserLessons)
    lessons:Lesson[];
}