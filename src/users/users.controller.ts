import { Body, Controller, Delete, Get, Param, Post, Put, UseGuards, UsePipes } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { Roles } from 'src/auth/roles-auth.decorator';
import { RolesGuard } from 'src/auth/roles.guard';
import { ValidationPipe } from 'src/pipes/validation.pipe';
import { addRoleDto } from './dto/add-role.dto';
import { BanUserDto } from './dto/ban-user.dto';
import { CreateUserDto } from './dto/create-user.dto';
import { StringDto } from './dto/string.dto';
import { User } from './users.model';
import { UsersService } from './users.service';
@ApiTags('Пользователи')
@Controller('users')
export class UsersController {

    constructor(private usersService: UsersService){}

    @ApiOperation({summary:'Создание пользователя'})
    @ApiResponse({status:200, type: [User] })
    @Post()
    create(@Body() userDto: CreateUserDto){
        return this.usersService.createUser(userDto);
    }

    @ApiOperation({summary:'Обновление пользователя'})
    @ApiResponse({status:200, type: [User] })
    @Put()
    update(@Body() userDto: CreateUserDto){
        return this.usersService.updateUser(userDto);
    }
    
    @ApiOperation({summary: 'Получить пользователя по email'})
    @ApiResponse({status: 200, type: [User]})
    @Roles("admin")
    @UseGuards(RolesGuard)
    @Post('/find')
    getByEmail(@Body() dto:StringDto) {
        let str:string = dto.email!;
        return this.usersService.getUsersByEmail(str);
    }

    @ApiOperation({summary: 'Получить пользователя по ID'})
    @ApiResponse({status: 200, type: [User]})
    @Get('/:value')
    getById(@Param('value') id:number) {
        return this.usersService.getUserById(id);
    }

    @ApiOperation({summary: 'Получить всех пользователей'})
    @ApiResponse({status: 200, type: [User]})
    @Roles("admin")
    @UseGuards(RolesGuard)
    @Get()
    getAll() {
        return this.usersService.getAllUsers();
    }

    @ApiOperation({summary:'Выдать роль'})
    @ApiResponse({status:200, type: [User] })
    @Roles("admin")
    @UseGuards(RolesGuard)
    @Post('/role')
    addRole(@Body() dto:addRoleDto){
        return this.usersService.addRole(dto);
    }

    @ApiOperation({summary:'Забанить пользователя'})
    @ApiResponse({status:200, type: [User] })
    @Roles("admin")
    @UseGuards(RolesGuard)
    @Post('/ban')
    ban(@Body() dto:BanUserDto){
        return this.usersService.ban(dto);
    }

    @ApiOperation({summary:'Удаление пользователя по почте'})
    @ApiResponse({status:200, type: [User] })
    @Roles("admin")
    @UseGuards(RolesGuard)
    @Delete()
    delete(@Body() dto:StringDto){
        let email:string = dto.email!;
        return this.usersService.delete(email);
    }
}
