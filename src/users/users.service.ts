import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { RolesService } from 'src/roles/roles.service';
import { addRoleDto } from './dto/add-role.dto';
import { BanUserDto } from './dto/ban-user.dto';
import { CreateUserDto } from './dto/create-user.dto';
import { StringDto } from './dto/string.dto';
import { User } from './users.model';

@Injectable()
export class UsersService {

    constructor(@InjectModel(User) private userRepository: typeof User, 
    private roleService:RolesService){}

    async createUser(dto: CreateUserDto){
        const user = await this.userRepository.create(dto);
        await user.save();
        return user;
    }
    
    async updateUser(dto: CreateUserDto){
        const user = await this.getUsersByEmail(dto.email);
        user.name = dto.name;
        user.surname = dto.surname;
        user.save();
        return user;
    }
    async getAllUsers(){
        const users = await this.userRepository.findAll({include:{all:true}});
        return users;
    }
    async getUsersByEmail(email: string){
        const user = await this.userRepository.findOne({where:{email}, include: {all:true}});
        return user;
    }
    async getUserById(id:number){
        const user = await this.userRepository.findByPk(id);
        return user;
    }
    async addRole(dto: addRoleDto){
        const user = await this.userRepository.findByPk(dto.userId);
        const role = await this.roleService.getRoleByValue(dto.value);
        if(role && user){
            await user.$add('role', role.id);
            return dto;
        }
        throw new HttpException('Роль или пользователь не были найдены!',HttpStatus.NOT_FOUND)
    }
    async ban(dto: BanUserDto) {
        const user = await this.userRepository.findByPk(dto.userId);
        if(!user){
            throw new HttpException('',HttpStatus.NOT_FOUND)
        }

        user.banned = true;
        user.banReason = dto.banReason;
        await user.save();
        return user;
    }

    async setToken(email: string, token:string) {
        const user = await this.getUsersByEmail(email);
        if(!user){
            throw new HttpException('',HttpStatus.NOT_FOUND)
        }
        user.token = token;
        await user.save();
        return user;
    }

    async delete(name: string): Promise<User | undefined> { 
        const user = await this.userRepository.findOne(
            {where: 
                {name}, 
            });
        user.destroy();
        return user;
    }
}
