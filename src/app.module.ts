import { Module } from "@nestjs/common";
import { ConfigModule } from "@nestjs/config";
import { SequelizeModule } from "@nestjs/sequelize";
import { User } from "./users/users.model";
import { UsersModule } from './users/users.module';
import { RolesModule } from './roles/roles.module';
import { Role } from "./roles/roles.model";
import { UserRoles } from "./roles/user-roles.model";
import { AuthModule } from './auth/auth.module';
import { LessonsModule } from './lessons/lessons.module';
import { Lesson } from "./lessons/lessons.model";
import { LessonGroup } from "./lessons/lessons-group.model";
import { FilesModule } from './files/files.module';
import { UserLessons } from "./lessons/user-lessons.model";
import { MailerModule } from '@nestjs-modules/mailer';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';
import { Questions } from "./questions/questions.model";
import { QuestionsModule } from "./questions/questions.module";
import { AnswersModule } from "./answers/answers.module";
import { Answers } from "./answers/answers.model";
import { MulterModule } from "@nestjs/platform-express";
@Module({
  controllers:[],  
  providers:[],
  imports: [
    MulterModule.register({
      dest: './static',
    }),
    ConfigModule.forRoot({
        envFilePath:'.env'
    }),
    SequelizeModule.forRoot({
        dialect: 'postgres',
        host: process.env.POSTGRES_HOST,
        port: Number(process.env.POSTGRES_PORT),
        username: process.env.POSTGRES_USER,
        password: process.env.POSTGRES_PASSWORD,
        database: process.env.POSTGRES_DB,
        models:[User, Role, UserRoles, Lesson, LessonGroup, UserLessons, Questions, Answers],
        autoLoadModels:true
    }),
      UsersModule,
      RolesModule,
      AuthModule,
      LessonsModule,
      FilesModule,
      QuestionsModule,
      AnswersModule,
      MailerModule.forRoot({
        transport: 'smtps://user@domain.com:pass@smtp.domain.com',
        defaults: {
          from: '"nest-modules" <modules@nestjs.com>',
        },
        template: {
          dir: __dirname + '/templates',
          adapter: new HandlebarsAdapter(),
          options: {
            strict: true,
          },
        },
      }),
    ]
})
export class AppModule {}