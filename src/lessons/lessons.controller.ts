import { Body, Controller, Delete, Get, Param, Post, Put, Query, Res, UploadedFile, UseInterceptors } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { join } from 'path';
import { Observable, of } from 'rxjs';
import { addUserDto } from './dto/add-user.dto';
import { CreateLessonDto } from './dto/create-lessons.dto';
import { StringDto } from './dto/string.dto';
import { LessonsService } from './lessons.service';
import { Express } from 'express';
import 'multer';
import { gradeUserDto } from './dto/grade-user.dto';

@ApiTags('Уроки')
@Controller('lessons')
export class LessonsController {
    constructor(private lessonsService:LessonsService){}

    @ApiOperation({summary:'Создание группы уроков'})
    @Post('/group')
    createGroup(@Body() dto:StringDto){
        return this.lessonsService.createGroup(dto);
    }
    @ApiOperation({summary:'Получить группы'})
    @Get('/group')
    getGroup(){
        return this.lessonsService.getGroup();
    }
    @ApiOperation({summary:'Получить группу по id'})
    @Get('/group/:id')
    getGroupById(@Param('id') id:number){
        return this.lessonsService.getGroupById(id);
    }

    @ApiOperation({summary:'Создание урока'})
    @Post()
    create(@Body() dto:CreateLessonDto){
        return this.lessonsService.createLesson(dto);
    }

    @ApiOperation({summary:'Привязка пользователя'})
    @Post('/addUser')
    addUser(@Body() dto:addUserDto){
        return this.lessonsService.userAssign(dto);
    }
    
    @ApiOperation({summary:'Обновление группы уроков'})
    @Put('/group/:id')
    updateGroup(@Param('id') id:number, @Body() dto:CreateLessonDto){
        return this.lessonsService.updateGroup(id, dto);
    }

    @ApiOperation({summary:'Обновление урока'})
    @Put()
    update(@Body() dto:CreateLessonDto){
        return this.lessonsService.updateLesson(dto);
    }

    @ApiOperation({summary:'Удаление урока по названию'})
    @Delete()
    delete(@Body() dto:StringDto){
        let name:string = dto.name!;
        return this.lessonsService.delete(name);
    }

    @ApiOperation({summary:'Удаление урока по ID'})
    @Delete('/:id')
    deleteById(@Param('id') id:number){
        return this.lessonsService.deleteById(id);
    }

    @ApiOperation({summary:'Удалениe группы уроков по id'})
    @Delete('/group/:id')
    deleteGroup(@Param('id') id:number){
        return this.lessonsService.deleteGroup(id);
    }

    @ApiOperation({summary:'Найти урок по названию'})
    @Post('/find')
    findByName(@Body() dto:StringDto){
        let name:string = dto.name!;
        return this.lessonsService.getLessonsByName(name);
    }
    
    @ApiOperation({summary:'Найти все уроки'})
    @Get()
    findAll(){
        return this.lessonsService.getAllLessons();
    }
    @ApiOperation({summary:'Найти урок по ID'})
    @Get('/:id')
    findById(@Param('id') id:number){
        return this.lessonsService.getLessonById(id);
    }

    @ApiOperation({summary:'Загрузить видео'})
    @Post('/upload/:id')
    @UseInterceptors(FileInterceptor('video'))
    uploadVideo(@Param('id') id:number, @UploadedFile() video: Express.Multer.File){
        return this.lessonsService.uploadVideo(id, video);
    }

    @ApiOperation({summary:'Скачать файл'})
    @Get('/download/:filename')
    download(@Res() res, @Param('filename') filename){
        return res.download("static/"+filename)
    }
    @ApiOperation({summary:'Показать видео'})
    @Get('/static/:filename')
    showVideo(@Res() res, @Param('filename') filename):Observable<Object>{
        return of(res.sendFile(join(process.cwd(), 'static/'+ filename)));
    }

    @ApiOperation({summary:'Загрузить домашку'})
    @Post('/uploadHomework')
    @UseInterceptors(FileInterceptor('homework'))
    uploadHomework(@Query('userid') userid:number,@Query('lessonid') lessonid:number, @UploadedFile() homework: Express.Multer.File){
        return this.lessonsService.uploadFile(userid, lessonid, homework);
    }

    @ApiOperation({summary:'Оценка за урок'})
    @Post('/grade')
    grade(@Body() dto:gradeUserDto){
        return this.lessonsService.setGrade(dto);
    }

    @ApiOperation({summary:'Смотреть домашку'})
    @Get('/homework')
    getHomework(){
        return this.lessonsService.getHomework();
    }

}
