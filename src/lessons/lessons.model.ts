import { ApiProperty } from "@nestjs/swagger";
import { BelongsTo, BelongsToMany, Column, DataType, ForeignKey, HasMany, Model, Table } from "sequelize-typescript";
import { Questions } from "src/questions/questions.model";
import { User } from "src/users/users.model";
import { LessonGroup } from "./lessons-group.model";
import { UserLessons } from "./user-lessons.model";

interface LessonCreationAttr{
    name: string,
    text: string
}

@Table({tableName:'lessons'})
export class Lesson extends Model<Lesson, LessonCreationAttr>{
    @ApiProperty({example:'1', description:'Уникальный идентификатор'})
    @Column({type: DataType.INTEGER, unique: true, autoIncrement: true, primaryKey: true})
    id: number;
    
    @ApiProperty({example:'Урок №1', description:'Название урока'})
    @Column({type: DataType.STRING, unique: true, allowNull:false})
    name:string;

    @ApiProperty({example:'Какой то текст', description:'Текст урока'})
    @Column({type: DataType.STRING, unique: true, allowNull:false})
    text:string;
    
    @ApiProperty({example:'b958685a-6c68-4002-a9a3-e7bcac17c569.mp4', description:'Ссылка на видео'})
    @Column({type: DataType.STRING})
    video_link: string;
    
    @ApiProperty({example:'true', description:'Активен или нет'})
    @Column({type: DataType.BOOLEAN, defaultValue: true})
    is_active:boolean;

    @BelongsToMany(() => User, ()=>UserLessons)
    users:User[];
    
    @ForeignKey(() => LessonGroup)
    @Column({type: DataType.INTEGER})
    userId:number;

    @BelongsTo(() => LessonGroup)
    group: LessonGroup;

    @HasMany(() => Questions)
    question:Questions[];
}