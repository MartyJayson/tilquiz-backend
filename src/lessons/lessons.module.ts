import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { FilesModule } from 'src/files/files.module';
import { QuestionsModule } from 'src/questions/questions.module';
import { User } from 'src/users/users.model';
import { LessonGroup } from './lessons-group.model';
import { LessonsController } from './lessons.controller';
import { Lesson } from './lessons.model';
import { LessonsService } from './lessons.service';
import { UserLessons } from './user-lessons.model';

@Module({
  controllers: [LessonsController],
  providers: [LessonsService],
  imports:[
    SequelizeModule.forFeature([User, Lesson, UserLessons, LessonGroup]),
    FilesModule,
  ],
  exports:[
    LessonsService
  ]
})
export class LessonsModule {}
