import { ApiProperty } from "@nestjs/swagger";
import { BelongsToMany, Column, DataType, ForeignKey, Model, Table } from "sequelize-typescript";
import { User } from "src/users/users.model";
import { Lesson } from "./lessons.model";

@Table({tableName:'user-lessons', createdAt: false, updatedAt:false})
export class UserLessons extends Model<UserLessons>{
    @Column({type: DataType.INTEGER, unique: true, autoIncrement: true, primaryKey: true})
    id: number;

    @ApiProperty({example:'10.0', description:'Оценка'})
    @Column({type: DataType.DOUBLE, defaultValue:0})
    grade:number;

    @ApiProperty({example:'homework.pdf', description:'Домашняя работа'})
    @Column({type: DataType.STRING})
    homework:string;

    @ForeignKey(() => Lesson)
    @Column({type: DataType.INTEGER})
    lessonId: number;
    
    @ForeignKey(() => User)
    @Column({type: DataType.INTEGER})
    userId: number;
    

    
}