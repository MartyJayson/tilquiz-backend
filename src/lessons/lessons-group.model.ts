import { ApiProperty } from "@nestjs/swagger";
import { BelongsTo, BelongsToMany, Column, DataType, ForeignKey, HasMany, Model, Table } from "sequelize-typescript";
import { Lesson } from "./lessons.model";
import { UserLessons } from "./user-lessons.model";

interface LessonCreationAttr{
    name: string
}

@Table({tableName:'lessons-group'})
export class LessonGroup extends Model<LessonGroup, LessonCreationAttr>{
    @ApiProperty({example:'1', description:'Уникальный идентификатор'})
    @Column({type: DataType.INTEGER, unique: true, autoIncrement: true, primaryKey: true})
    id: number;
    
    @ApiProperty({example:'Урок №1', description:'Название группы уроков'})
    @Column({type: DataType.STRING, unique: true, allowNull:false})
    name:string;

    @HasMany(() => Lesson)
    lesson:Lesson[];
}