import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { FilesService } from 'src/files/files.service';
import { User } from 'src/users/users.model';
import { addUserDto } from './dto/add-user.dto';
import { CreateLessonDto } from './dto/create-lessons.dto';
import { gradeUserDto } from './dto/grade-user.dto';
import { homeworkDto } from './dto/homework.dto';
import { StringDto } from './dto/string.dto';
import { LessonGroup } from './lessons-group.model';
import { Lesson } from './lessons.model';
import { UserLessons } from './user-lessons.model';

@Injectable()
export class LessonsService {

    constructor(@InjectModel(Lesson) private lessonRepository: typeof Lesson,
                @InjectModel(LessonGroup) private lessonGroupRepository: typeof LessonGroup, 
                @InjectModel(User) private userRepository: typeof User, 
                @InjectModel(UserLessons) private userLessonRepository: typeof UserLessons, 
                private fileService:FilesService){}

    async userAssign(dto:addUserDto){
        const user = this.userRepository.findByPk(dto.userId);
        const lesson = this.userRepository.findByPk(dto.lessonId);
        
        if(user && lesson){
            (await user).$add('lesson', dto.lessonId);
            return dto;
        }
        throw new HttpException('Урок или пользователь не были найдены!',HttpStatus.NOT_FOUND);
    }     
    async GradeAssign(dto:gradeUserDto){
        const user = this.userRepository.findByPk(dto.userId);
        const lesson = this.userRepository.findByPk(dto.lessonId);
        
        if(user && lesson){
            (await user).$add('grade', dto.grade);
            return dto;
        }
        throw new HttpException('Урок или пользователь не были найдены!',HttpStatus.NOT_FOUND)
    }       
    async createLesson(dto: CreateLessonDto){
        const lesson = await this.lessonRepository.create(dto);
        return lesson;
    }
    async createGroup(dto: StringDto){
        const lessonGroup = await this.lessonGroupRepository.create(dto);
        return lessonGroup;
    }
    async getGroup(){
        const lessonGroup = await this.lessonGroupRepository.findAll({include:{all:true}});
        return lessonGroup;
    }
    async updateLesson(dto: CreateLessonDto){
        const lesson = await this.getLessonsByName(dto.name);
        lesson.name = dto.name;
        lesson.text = dto.text;
        lesson.save();
        return lesson;
    }
    async updateGroup(id:number, dto: StringDto){
        const lesson = await this.getGroupById(id);
        lesson.name = dto.name;
        lesson.save();
        return lesson;
    }
    async delete(name: string): Promise<Lesson | undefined> { 
        const lesson = await this.lessonRepository.findOne(
            {where: 
                {name}, 
            });
        lesson.destroy();
        return lesson;
    }
    async deleteById(id: number){ 
        const lesson = await this.lessonRepository.findByPk(id);
        lesson.destroy();
        return "Deleted";
    }
    async deleteGroup(id:number): Promise<LessonGroup | undefined> { 
        const lesson = await this.getGroupById(id);
        lesson.destroy();
        return lesson;
    }

    async getLessonsByName(name: string): Promise<Lesson | undefined> {
        const lesson = await this.lessonRepository.findOne(
            {where: 
                {name: name}, 
            });
        return lesson;
    }
    async getGroupById(id:number){
        const group = await this.lessonGroupRepository.findByPk(id)
        return group;
    }
    async getAllLessons(){
        const lesson = await this.lessonRepository.findAll({include:{all:true}});
        return lesson;
    }
    async getLessonById(id:number){
        const lesson = await this.lessonRepository.findByPk(id);
        return lesson;
    }
    async uploadVideo(id: number, video: any) {
        const fileName = await this.fileService.createFile(video);
        const lesson = await this.lessonRepository.findByPk(id);
        lesson.set('video_link', fileName);
        lesson.save();
        return lesson;
    }
    async uploadFile(userId: number, lessonId: number, file: any){
        const fileName = await this.fileService.createFile(file);
        var homework = await new UserLessons();
        let ul = await this.userLessonRepository.findOne({where:{lessonId,userId}, include: {all:true}});
        if(ul == null)
        {
            ul = await this.userLessonRepository.create(homework);
            ul.set('userId', userId);
            ul.set('lessonId', lessonId);    
        }
        ul.set('homework', fileName);   
        ul.save();
        return ul;   
        
    }
    async setGrade(dto:gradeUserDto){
        var lessonId = dto.lessonId;
        var userId = dto.userId;
        let lesson = await this.userLessonRepository.findOne({where:{lessonId,userId}, include: {all:true}});
        if(lesson == null)
        {
            lesson = await this.userLessonRepository.create(dto);
            lesson.set('userId', userId);
            lesson.set('lessonId', lessonId);    
        }
        lesson.set('grade',dto.grade);
        lesson.save();
        return lesson;
    }
    async getHomework(){
        const lesson = await this.userLessonRepository.findAll({include:{all:true}});
        return lesson;
    }

}
