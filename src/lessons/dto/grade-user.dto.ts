export class gradeUserDto{
    readonly lessonId: number;
    readonly userId: number;
    readonly grade: number;
}