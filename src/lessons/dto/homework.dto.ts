import {ApiProperty} from "@nestjs/swagger";
import { LessonGroup } from "../lessons-group.model";

export class homeworkDto {

    @ApiProperty({example: '1', description: 'id of lesson'})
     lessonId: number;

    @ApiProperty({example: '2', description: 'id of user'})
     userId: number;
    
    @ApiProperty({example:'homework.pdf', description:'Домашняя работа'})
     homework:string;

}