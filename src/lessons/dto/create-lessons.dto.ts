import {ApiProperty} from "@nestjs/swagger";
import {IsEmail, IsString, Length} from "class-validator";
import { LessonGroup } from "../lessons-group.model";

export class CreateLessonDto {

    @ApiProperty({example: 'Урок #1', description: 'Название урока'})
    @IsString({message: 'Должно быть строкой'})
    @Length(4, 36, {message: 'Не меньше 4 и не больше 36'})
    readonly name: string;

    @ApiProperty({example: 'На этом уроке мы пройдем ...', description: 'Описание урока'})
    @IsString({message: 'Должно быть строкой'})
    readonly text: string;
    
    @ApiProperty({example: '1', description: 'Группа уроков'})
    readonly lessonGroup: LessonGroup;
}