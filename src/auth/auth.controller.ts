import { Body, Controller, Param, Post } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { StringDto } from 'src/users/dto/string.dto';
import { AuthService } from './auth.service';
import { ConfrimDto } from './dto/confirm.dto';
import { newPasswordDto } from './dto/newpassword.dto';


@ApiTags('Авторизация')
@Controller('auth')
export class AuthController {

    constructor(private authService:AuthService){}

    @ApiOperation({summary:'Войти'})
    @Post('/login')
    login(@Body() userDto: CreateUserDto){
        return this.authService.login(userDto);
    }
    
    @ApiOperation({summary:'Зарегистрироваться'})
    @Post('/registration')
    registration(@Body() userDto: CreateUserDto){
        return this.authService.registration(userDto);
    }

    @ApiOperation({summary:'Сбросить пароль'})
    @Post('/reset')
    updatePassword(@Body() userDto: StringDto){
        return this.authService.updatePassword(userDto);
    }

    @ApiOperation({summary:'Подтверждение'})
    @Post('/confirm')
    confirm(@Body() dto:ConfrimDto){
        return this.authService.confirm(dto.token,dto.email);
    }
    
    @ApiOperation({summary:'Новый пароль'})
    @Post('/newPassword')
    newPassword(@Body() dto:newPasswordDto){
        return this.authService.newPassword(dto.email,dto.password);
    }
}
