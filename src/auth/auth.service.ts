import { HttpException, HttpStatus, Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { UsersService } from 'src/users/users.service';
import * as bcrypt from 'bcryptjs'
import { User } from 'src/users/users.model';
import { MailService } from 'src/mail/mail.service';
import { StringDto } from 'src/users/dto/string.dto';
@Injectable()
export class AuthService {

    constructor(private userService:UsersService,
                private jwtService:JwtService,
                private mailService: MailService){}

    async login(userDto: CreateUserDto){
        const user = await this.validateUser(userDto);
        return this.generateToken(user);
    }

    async updatePassword(userDto: StringDto) {
        const token = Math.floor(1000 + Math.random() * 9000).toString();
        await this.mailService.sendUserConfirmation(userDto, token);
        await this.userService.setToken(userDto.email ,token)
    }

    async confirm(token:string, email:string){
        const candidate = await this.userService.getUsersByEmail(email);
        if(candidate.token = token)
            return true;
        else
            throw new HttpException('Ошибка', HttpStatus.BAD_REQUEST)
    }

    async newPassword(email:string, newPassword:string){
        const candidate = await this.userService.getUsersByEmail(email);
        candidate.password = await bcrypt.hash(newPassword, 5);
        candidate.save();
        return candidate;
    }

    async registration(userDto: CreateUserDto){
        const candidate = await this.userService.getUsersByEmail(userDto.email);
        if(candidate){
            throw new HttpException('Пользователь с таким email уже существует', HttpStatus.BAD_REQUEST)
        }
        const hashPassword = await bcrypt.hash(userDto.password, 5);
        const user = await this.userService.createUser({...userDto, password: hashPassword});
        return this.generateToken(user);
    }
    private async generateToken(user:User){
        const payload = {email: user.email, id: user.id, roles: user.roles};
        return {
            id: user.id,
            token: this.jwtService.sign(payload)
        }
    }
    
    private async validateUser(userDto: CreateUserDto) {
       const user = await this.userService.getUsersByEmail(userDto.email);
       const passwordEquals = await bcrypt.compare(userDto.password, user.password);
       if(user && passwordEquals){
           return user;
       }
       throw new UnauthorizedException({message: 'Неправильный логин или пароль!'})
    }
}
