import {ApiProperty} from "@nestjs/swagger";
import { IsString } from "class-validator";

export class CreateAnswerDto {

    @ApiProperty({example: 'Какой то там ответ', description: 'Вариант ответа для вопроса'})
    @IsString({message: 'Должно быть строкой'})
    readonly text: string;

    @ApiProperty({example:'true/false', description:'Правильный ли ответ'})
    right: boolean;
}