import { Module } from "@nestjs/common";
import { SequelizeModule } from "@nestjs/sequelize";
import { AnswersController } from "./answers.controller"
import { AnswersService } from "./answers.service";
import { Answers } from "./answers.model";
import { Questions } from "src/questions/questions.model";
import { QuestionsModule } from "src/questions/questions.module";

@Module({
    controllers: [AnswersController],
    providers: [AnswersService],
    imports:[
      SequelizeModule.forFeature([Answers, Questions]),
      QuestionsModule
    ],
    exports:[
        AnswersService
    ]
  })
export class AnswersModule{}