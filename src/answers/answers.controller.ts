import { Body, Controller, Delete, Get, Param, Post, Put } from "@nestjs/common";
import { ApiOperation, ApiResponse, ApiTags } from "@nestjs/swagger";
import { Answers } from "./answers.model";
import { AnswersService } from "./answers.service";
import { CreateAnswerDto } from "./dto/create-answer.dto";

@ApiTags('Ответы')
@Controller('answers')
export class AnswersController{
    constructor(private answersService:AnswersService){}

    @ApiOperation({summary:'Создание ответа'})
    @ApiResponse({status:200, type: [Answers] })
    @Post('/:questionId')
    create(@Param('questionId') questionId:number, @Body() dto: CreateAnswerDto){
        return this.answersService.createAnswer(questionId, dto);
    }

    @ApiOperation({summary:'Создание ответа'})
    @ApiResponse({status:200, type: [Answers] })
    @Put('/:id')
    update(@Param('id') id:number, @Body() dto: CreateAnswerDto){
        return this.answersService.updateAnswer(id, dto);
    }

    @ApiOperation({summary: 'Получить все ответы'})
    @ApiResponse({status: 200, type: [Answers]})
    @Get()
    getAll() {
        return this.answersService.getAllAnswers();
    }

    @ApiOperation({summary:'Удаление ответа по ид'})
    @ApiResponse({status:200, type: [Answers] })
    @Delete()
    delete(@Param() id:number){
        return this.answersService.delete(id);
    }
}