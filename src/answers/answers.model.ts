import { ApiProperty } from "@nestjs/swagger";
import { BelongsTo, BelongsToMany, Column, DataType, ForeignKey, HasMany, Model, Table } from "sequelize-typescript";
import { Questions } from "src/questions/questions.model";

interface AnswerCreationAttr{
    name: string
}

@Table({tableName:'answers'})
export class Answers extends Model<Answers, AnswerCreationAttr>{
    @ApiProperty({example:'1', description:'Уникальный идентификатор'})
    @Column({type: DataType.INTEGER, unique: true, autoIncrement: true, primaryKey: true})
    id: number;

    @ApiProperty({example:'Какой то ответ', description:'Содержание ответа'})
    @Column({type: DataType.STRING})
    text: string;
    @ApiProperty({example:'true/false', description:'Правильный ли ответ'})
    @Column({type: DataType.BOOLEAN})
    right: boolean;
    @ForeignKey(() => Questions)
    @Column({type: DataType.INTEGER})
    questionId:number;
    @BelongsTo(() => Questions)
    question: Questions;
}