import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/sequelize";
import { Answers } from "./answers.model";
import { CreateAnswerDto } from "./dto/create-answer.dto";

@Injectable()
export class AnswersService{
    constructor(@InjectModel(Answers) private answersRepository: typeof Answers){}
    
    async createAnswer(questionId:number, dto:CreateAnswerDto){
        const answers = await this.answersRepository.create(dto);
        answers.set('questionId', questionId);
        answers.save();
        return answers;
    }
    async updateAnswer(id:number, dto:CreateAnswerDto){
        const answer = await this.answersRepository.findByPk(id);
        answer.text = dto.text;
        answer.right = dto.right;
        answer.save();
        return answer;
    }
    async getAllAnswers(){
        const answers = await this.answersRepository.findAll({include:{all:true}});
        return answers;
    }
    async delete(id: number): Promise<Answers | undefined> { 
        const answers = await this.answersRepository.findByPk(id);
        if(!answers){
            throw new HttpException('',HttpStatus.NOT_FOUND)
        }
        answers.destroy();
        return answers;
    }
}