import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import * as fs from 'fs';
import * as path from 'path';
import * as uuid from 'uuid';
import { Express } from 'express';
import 'multer';

@Injectable()
export class FilesService {

    async createFile(file: Express.Multer.File){
        try{
            const name = file.originalname;
            const format = name.split('.').pop();
            const fileName = uuid.v4() + '.' + format;
            const filePath = path.resolve('static')
            if(!fs.existsSync(filePath)){
                fs.mkdirSync(filePath, {recursive:true})
            }
            fs.writeFileSync(path.join(filePath, fileName),file.buffer);
            return fileName;
        }
        catch(e){
            throw new HttpException('Произошла ошибка при записи файла', HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }
}
