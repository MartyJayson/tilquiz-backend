import { MailerService } from '@nestjs-modules/mailer';
import { Injectable } from '@nestjs/common';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { StringDto } from 'src/users/dto/string.dto';
import { UsersService } from 'src/users/users.service';

@Injectable()
export class MailService {
  constructor(private mailerService: MailerService, private usersService: UsersService) {}

  async sendUserConfirmation(userObject: StringDto, token: string) {
    const url = `http:tilquiz.kz/reset-password?token=${token}`+`&email=${userObject.email}`;
    await this.mailerService.sendMail({
      to: userObject.email,
      // from: '"Support Team" <support@example.com>', // override default from
      subject: 'Password reset request from TilQuiz!',
      template: './src/mail/templates/confirmation.hbs', // `.hbs` extension is appended automatically
      context: { // ✏️ filling curly brackets with content
        url: url.toString(),
        token
      },
    });
  }
}
