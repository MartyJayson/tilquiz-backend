import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/sequelize";
import { CreateQuestionDto } from "./dto/create-question.dto";
import { ScoreDto } from "./dto/score.dto";
import { Questions } from "./questions.model";

@Injectable()
export class QuestionsService{
    constructor(@InjectModel(Questions) private questionRepository: typeof Questions){}
    
    async createQuestion(lessonId:number, dto: CreateQuestionDto){
        const question = await this.questionRepository.create(dto);
        question.set('lessonId', lessonId);
        question.save();
        return question;
    }

    async getQuestionByName(text: string): Promise<Questions | undefined> {
        const lesson = await this.questionRepository.findOne(
            {where: 
                {text: text}, 
            });
        return lesson;
    }
    async updateQuestion(id:number, dto: CreateQuestionDto){
        const question = await this.questionRepository.findByPk(id);
        question.text = dto.text;
        question.save();
        return question;
    }

    async score(id:number, dto: ScoreDto){
        const question = await this.questionRepository.findByPk(id);
        question.grade = dto.score;
        question.save();
        return question;
    }
    async getAllQuestions(){
        const questions = await this.questionRepository.findAll({include:{all:true}});
        return questions;
    }

    async delete(id: number): Promise<Questions | undefined> { 
        const questions = await this.questionRepository.findByPk(id);
        if(!questions){
            throw new HttpException('',HttpStatus.NOT_FOUND)
        }
        questions.destroy();
        return questions;
    }
}