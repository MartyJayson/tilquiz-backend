import {ApiProperty} from "@nestjs/swagger";
import { IsString } from "class-validator";

export class CreateQuestionDto {

    @ApiProperty({example: 'Как будет по-казахски ...?', description: 'Сам вопрос'})
    @IsString({message: 'Должно быть строкой'})
    readonly text: string;
}