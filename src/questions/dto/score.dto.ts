import {ApiProperty} from "@nestjs/swagger";
import { IsString } from "class-validator";

export class ScoreDto {

    @ApiProperty({example: '10', description: 'Баллы за вопрос'})
    readonly score: number;
}