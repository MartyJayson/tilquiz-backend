import { Body, Controller, Delete, Get, Param, Post, Put } from "@nestjs/common";
import { ApiOperation, ApiResponse, ApiTags } from "@nestjs/swagger";
import { CreateQuestionDto } from "./dto/create-question.dto";
import { ScoreDto } from "./dto/score.dto";
import { Questions } from "./questions.model";
import { QuestionsService } from "./questions.service";

@ApiTags('Вопросы')
@Controller('questions')
export class QuestionsController{
    constructor(private questionService:QuestionsService){}

    @ApiOperation({summary:'Создание вопроса'})
    @ApiResponse({status:200, type: [Questions] })
    @Post('/:lessonId')
    create(@Param('lessonId') lessonId:number, @Body() questionDto: CreateQuestionDto){
        return this.questionService.createQuestion(lessonId, questionDto);
    }

    @ApiOperation({summary:'Создание вопроса'})
    @ApiResponse({status:200, type: [Questions] })
    @Put('/:id')
    update(@Param('id') id:number, @Body() questionDto: CreateQuestionDto){
        return this.questionService.updateQuestion(id, questionDto);
    }

    @ApiOperation({summary:'Оценка вопроса'})
    @ApiResponse({status:200, type: [Questions] })
    @Post('/:id')
    scoreQuestion(@Param('id') id:number, @Body() scoreDto: ScoreDto){
        return this.questionService.score(id, scoreDto);
    }
    @ApiOperation({summary: 'Получить все вопросы'})
    @ApiResponse({status: 200, type: [Questions]})
    @Get()
    getAll() {
        return this.questionService.getAllQuestions();
    }

    @ApiOperation({summary:'Удаление ответа по ид'})
    @ApiResponse({status:200, type: [Questions] })
    @Delete('/:id')
    delete(@Param('id') id:number){
        return this.questionService.delete(id);
    }
}