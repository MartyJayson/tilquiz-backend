import { Module } from "@nestjs/common";
import { SequelizeModule } from "@nestjs/sequelize";
import { Lesson } from "src/lessons/lessons.model";
import { LessonsModule } from "src/lessons/lessons.module";
import { QuestionsController } from "./questions.controller";
import { Questions } from "./questions.model";
import { QuestionsService } from "./questions.service";

@Module({
    controllers: [QuestionsController],
    providers: [QuestionsService],
    imports:[
      SequelizeModule.forFeature([Questions, Lesson]),
      LessonsModule
    ],
    exports:[
        QuestionsService
    ]
  })
export class QuestionsModule{}