import { ApiProperty } from "@nestjs/swagger";
import { BelongsTo, BelongsToMany, Column, DataType, ForeignKey, HasMany, Model, Table } from "sequelize-typescript";
import { Answers } from "src/answers/answers.model";
import { Lesson } from "src/lessons/lessons.model";

interface QuestionCreationAttr{
    text: string
}

@Table({tableName:'questions'})
export class Questions extends Model<Questions, QuestionCreationAttr>{
    @ApiProperty({example:'1', description:'Уникальный идентификатор'})
    @Column({type: DataType.INTEGER, unique: true, autoIncrement: true, primaryKey: true})
    id: number;

    @ApiProperty({example:'Кто? Что?', description:'Содержание вопроса'})
    @Column({type: DataType.STRING})
    text: string;

    @ApiProperty({example:'1', description:'Стоимость вопроса'})
    @Column({type: DataType.INTEGER, defaultValue:1})
    grade:number;
    
    @ForeignKey(() => Lesson)
    @Column({type: DataType.INTEGER})
    lessonId:number;
    @BelongsTo(() => Lesson)
    lesson: Lesson;

    @HasMany(() => Answers)
    answers:Answers[];
}
