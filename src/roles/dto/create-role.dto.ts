import { IsString, Length } from "class-validator";

export class CreateRoleDto{
    @IsString({message: 'Должно быть строкой'})
    @Length(4, 30, {message: 'Не меньше 4 и не больше 30'})
    readonly value:string;
    @IsString({message: 'Должно быть строкой'})
    @Length(4, 100, {message: 'Не меньше 4 и не больше 100'})
    readonly description: string
}