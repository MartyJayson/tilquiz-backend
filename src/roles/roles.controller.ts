import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { CreateRoleDto } from './dto/create-role.dto';
import { StringDto } from './dto/string.dto';
import { RolesService } from './roles.service';

@ApiTags('Роли')
@Controller('roles')
export class RolesController {
    constructor(private roleService:RolesService){}

    @ApiOperation({summary:'Создание роли'})
    @Post()
    create(@Body() dto:CreateRoleDto){
        return this.roleService.createRole(dto);
    }

    @ApiOperation({summary:'Найти роль по value'})
    @Get('/:value')
    getByValue(@Param('value') value:string){
        return this.roleService.getRoleByValue(value);
    }
    
    @ApiOperation({summary:'Удаление роли по названию'})
    @Delete()
    delete(@Body() dto:StringDto){
        let value:string = dto.value!;
        return this.roleService.delete(value);
    }
}
